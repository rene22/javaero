package git.test.javaero;

import java.util.Collection;
import java.util.Map;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LsRemoteCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class GitTags1 {

	// private static final String USERNAME = "mdren";
	// private static final String PW = "Re.gan6.u";
	// private static final String REMOTE_URL =
	// "https://github.com/Mdcms/mdcms.git";
	// private static final String REMOTE_URL =
	// "https://bitbucket.org/midrangedynamics/mdcmst8.git";
	// private static final String REMOTE_URL =
	// "https://github.com/rene22/logan.git";
	private static final String REMOTE_URL = "https://bitbucket.org/rene22/javaero.git";

	/*
	 * git.checkout().setCreateBranch( true ).setName( "my-branch"
	 * ).setStartPoint( "refs/tags/my-tag" ).call();
	 * 
	 * https://stackoverflow.com/questions/33762575/load-content-from-tag-with-jgit
	 */

	/*
	 * Git git = new Git(db); Iterable<RevCommit> log = git.log().call();
	 */

	private static final String USERNAME = "rene22";
	private static final String PW = "Reygan66u";

	public static void main(String[] args) throws GitAPIException {
		// then clone
		System.out.println("Listing remote repository " + REMOTE_URL);

		LsRemoteCommand lsRemoteCommand = Git.lsRemoteRepository();
		CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(
				USERNAME, PW);
		lsRemoteCommand.setCredentialsProvider(credentialsProvider);

		// Collection<Ref> refs = lsRemoteCommand.setHeads(true).setTags(true)
		// .setRemote(REMOTE_URL).call();

		Collection<Ref> refs = lsRemoteCommand.setHeads(true).setTags(true)
				.setRemote(REMOTE_URL).call();

		for (Ref ref : refs) {
			System.out.println("Ref: " + ref);
		}

		final Map<String, Ref> map = lsRemoteCommand.setHeads(true)
				.setTags(true).setRemote(REMOTE_URL).callAsMap();

		System.out.println("As map");
		for (Map.Entry<String, Ref> entry : map.entrySet()) {
			System.out.println("Key: " + entry.getKey() + ", Ref: "
					+ entry.getValue());
		}

		refs = lsRemoteCommand.setRemote(REMOTE_URL).call();

		// Git.lsRemoteRepository().getRepository().

		System.out.println("All refs");
		for (Ref ref : refs) {
			System.out.println("Ref: " + ref);
		}
	}
}
